package roman

import "strings"

func RomanNumeral(arabic int) string {
	var num strings.Builder
	for arabic > 0 {
		switch {
		case arabic > 3999:
			num.WriteString("Limit Exceeded")
			arabic = 0
		case arabic > 999:
			num.WriteString("M")
			arabic -= 1000
		case arabic > 899:
			num.WriteString("CM")
			arabic -= 900
		case arabic > 499:
			num.WriteString("D")
			arabic -= 500
		case arabic > 399:
			num.WriteString("CD")
			arabic -= 400
		case arabic > 99:
			num.WriteString("C")
			arabic -= 100
		case arabic > 89:
			num.WriteString("XC")
			arabic -= 90
		case arabic > 49:
			num.WriteString("L")
			arabic -= 50
		case arabic > 39:
			num.WriteString("XL")
			arabic -= 40
		case arabic > 9:
			num.WriteString("X")
			arabic -= 10
		case arabic > 8:
			num.WriteString("IX")
			arabic -= 9
		case arabic > 4:
			num.WriteString("V")
			arabic -= 5
		case arabic > 3:
			num.WriteString("IV")
			arabic -= 4
		default:
			num.WriteString("I")
			arabic--
		}
	}
	return num.String()
}

func Arabic(roman string) int {
	if roman == "I" {
		return 1
	}
	return 100
}
