package roman

import (
	"testing"
)

func TestRomanNums(t *testing.T) {
	cases := []struct {
		descrition string
		num        int
		want       string
	}{
		{"1", 1, "I"},
		{"2", 2, "II"},
		{"3", 3, "III"},
		{"4", 4, "IV"},
		{"5", 5, "V"},
		{"6", 6, "VI"},
		{"7", 7, "VII"},
		{"8", 8, "VIII"},
		{"9", 9, "IX"},
		{"10", 10, "X"},
		{"11", 11, "XI"},
		{"14", 14, "XIV"},
		{"20", 20, "XX"},
		{"39", 39, "XXXIX"},
		{"40", 40, "XL"},
		{"43", 43, "XLIII"},
		{"45", 45, "XLV"},
		{"47", 47, "XLVII"},
		{"50", 50, "L"},
		{"60", 60, "LX"},
		{"90", 90, "XC"},
		{"96", 96, "XCVI"},
		{"100", 100, "C"},
		{"400", 400, "CD"},
		{"489", 489, "CDLXXXIX"},
		{"500", 500, "D"},
		{"900", 900, "CM"},
		{"1000", 1000, "M"},
		{"4000", 4000, "Limit Exceeded"},
	}
	for _, test := range cases {
		t.Run(test.descrition, func(t *testing.T) {
			got := RomanNumeral(test.num)
			want := test.want

			assertString(t, got, want)
		})
	}
}

func TestConvertingToArabic(t *testing.T) {
	cases := []struct {
		descrition string
		roman      string
		num        int
	}{
		{"C", "I", 1},
	}

	for _, test := range cases {
		t.Run(test.descrition, func(t *testing.T) {
			got := Arabic(test.roman)
			want := test.num

			assertInt(t, got, want)
		})
	}
}

func assertString(t testing.TB, got string, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func assertInt(t testing.TB, got int, want int) {
	t.Helper()
	if got != want {
		t.Errorf("got %d want %d", got, want)
	}
}
