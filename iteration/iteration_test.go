package iteration

import "testing"

func TestRepeat(t *testing.T){
	repeated := Repeat("a", 5)
	expect := "aaaaa"

	assertString(t, repeated, expect)
}
func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 5)
	}
}


func assertString(t testing.TB, got string, want string){
	t.Helper()
	if(got != want){
		t.Errorf("got %q want %q", got, want)
	}
}
