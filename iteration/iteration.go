package iteration


func Repeat(s string, t int) string {
	var repeat string
	for i:=0; i<t; i++{
		repeat += s
	}
	return repeat
}