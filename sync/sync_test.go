package counter

import (
	"sync"
	"testing"
)

func TestCounter(t *testing.T) {
	t.Run("Incrementing the counter 3 times, leaves it at 3", func(t *testing.T) {
		counter := Counter{}
		counter.Inc()
		counter.Inc()
		counter.Inc()

		got := counter.Value()
		want := 3

		assertInt(t, got, want)

	})
	t.Run("Increment concurrently", func(t *testing.T) {
		counter := Counter{}

		wantedCounts := 1000

		var wg sync.WaitGroup
		wg.Add(wantedCounts)

		for i := 0; i < wantedCounts; i++ {
			go func() {
				counter.Inc()
				wg.Done()
			}()
		}
		wg.Wait()

		assertInt(t, counter.Value(), wantedCounts)

	})
}

func assertInt(t testing.TB, got int, want int) {
	t.Helper()
	if got != want {
		t.Errorf("got %d want %d", got, want)
	}
}
