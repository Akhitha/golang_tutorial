package methods

import "math"

type Shape interface {
	Area() float64
}

type Rectangle struct {
	Width  float64
	Height float64
}

type Circle struct {
	Radius float64
}

func (r Rectangle) Area() float64 {
	x := r.Width
	y := r.Height
	return x * y
}

func (c Circle) Area() float64 {
	r := c.Radius
	return math.Pi * r * r
}

func (r Rectangle) Perimeter() float64 {
	x := r.Width
	y := r.Height
	return (x + y) * 2
}

func (c Circle) Perimeter() float64 {
	r := c.Radius

	return math.Pi * r * 2
}
