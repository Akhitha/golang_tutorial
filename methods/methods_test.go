package methods

import "testing"

func TestPerimeter(t *testing.T) {
	t.Run("Rectangle", func(t *testing.T) {
		rectangle := Rectangle{10.0, 10.0}
		got := rectangle.Perimeter()
		want := 40.0

		assertFloat(t, got, want)
	})
	t.Run("Circle", func(t *testing.T) {
		circle := Circle{10.0}
		got := circle.Perimeter()
		want := 62.83185307179586476925286766559

		assertFloat(t, got, want)
	})

}

func TestArea(t *testing.T) {
	checkArea := func(t *testing.T, shape Shape, want float64) {
		t.Helper()

		got := shape.Area()
		assertFloat(t, got, want)

	}
	t.Run("Rectangle", func(t *testing.T) {
		rectangle := Rectangle{12.0, 4.5}
		want := 54.0

		checkArea(t, rectangle, want)
	})
	t.Run("Circle", func(t *testing.T) {
		circle := Circle{10.0}
		want := 314.1592653589793

		checkArea(t, circle, want)
	})
}

func assertFloat(t testing.TB, got float64, want float64) {
	t.Helper()

	if got != want {
		t.Errorf("got %.2f want %.2f", got, want)
	}
}
