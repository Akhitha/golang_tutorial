package maps

import "errors"

type Dictionary map[string]string

var ErrNotFound = errors.New("The key not available")
var ErrAlreadyExistingKey = errors.New("The key already available")

func (d Dictionary) Search(key string) (string, error) {
	value, status := d[key]

	if !status {
		return "", ErrNotFound

	}
	return value, nil
}

func (d Dictionary) Add(key string, value string) (Dictionary, error) {

	_, err := d.Search(key)
	if err == ErrNotFound {
		d[key] = value
		return d, nil
	} else {
		return d, ErrAlreadyExistingKey
	}

}

func (d Dictionary) Delete(key string) (Dictionary, error) {
	_, err := d.Search(key)

	if err == nil {
		delete(d, key)
		return d, nil
	} else {
		return d, ErrNotFound
	}

}
