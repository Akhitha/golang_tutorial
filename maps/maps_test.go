package maps

import "testing"

func TestSearch(t *testing.T) {
	dictionary := Dictionary{"test": "this is a test string"}

	t.Run("Already available key", func(t *testing.T) {
		got, err := dictionary.Search("test")
		want := "this is a test string"

		assertString(t, got, want)
		assertNotError(t, err)
	})
	t.Run("Unavailable key", func(t *testing.T) {
		_, err := dictionary.Search("unavailable")
		want := "The key not available"

		assertError(t, err)

		assertString(t, err.Error(), want)
	})
}

func TestAdd(t *testing.T) {
	t.Run("Adding to an empt dictionary", func(t *testing.T) {
		dictionary := Dictionary{}
		dictionary.Add("key", "value")

		want := "value"

		got, err := dictionary.Search("key")

		assertNotError(t, err)
		assertString(t, got, want)
	})
	t.Run("Adding to an existing key", func(t *testing.T) {
		dictionary := Dictionary{"key": "value"}

		dictionary, err := dictionary.Add("key", "new value")

		got, searchErr := dictionary.Search("key")
		want := "value"

		assertError(t, err)
		assertNotError(t, searchErr)
		assertString(t, got, want)
	})
}

func TestDelete(t *testing.T) {
	t.Run("Delete an existing Key, Value pare", func(t *testing.T) {
		key := "key"
		dictionary := Dictionary{key: "Value"}

		dictionary, errDelete := dictionary.Delete(key)

		_, errSearch := dictionary.Search(key)

		assertNotError(t, errDelete)
		assertError(t, errSearch)

	})
}

func assertString(t testing.TB, got string, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func assertError(t testing.TB, err error) {
	t.Helper()

	if err == nil {
		t.Errorf("An error required")
	}
}

func assertNotError(t testing.TB, err error) {
	t.Helper()

	if err != nil {
		t.Errorf("A nil required, but an error available")
	}
}
