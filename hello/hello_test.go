package main


import "testing"


func TestHello(t *testing.T){
	t.Run("With Name", func(t *testing.T){
		got:=Hello("Chris", "")
		want:="Hello Chris"

		assertCorrectMessage(t,got, want)
	})
	t.Run("Without Name", func(t *testing.T){
		got:=Hello("", "")
		want:="Hello World"

		assertCorrectMessage(t, got, want)
	})
	t.Run("In Spanish", func(t *testing.T){
		got:=Hello("Elodie", "Spanish")
		want:="Hola Elodie"

		assertCorrectMessage(t, got, want)
	})
	t.Run("In French", func(t *testing.T){
		got:=Hello("Mylan", "French")
		want:="Bonjour Mylan"

		assertCorrectMessage(t, got, want)
	})
}

func assertCorrectMessage(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}