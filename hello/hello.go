package main

import "fmt"

const prefix_e = "Hello "
const prefix_s = "Hola "
const prefix_f = "Bonjour "
func Hello(s string, l string) string {
	if(s == ""){
		s = "World"
	}

	return Prefix(l) + s	
}

func Prefix(l string)(prefix string){
	switch l{
	case "Spanish":
		prefix=prefix_s
	case "French":
		prefix=prefix_f
	default:
		prefix=prefix_e
	}
	return
}

func main() {
	fmt.Println(Hello("World", ""))
}