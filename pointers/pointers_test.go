package pointers

import "testing"

func TestWallet(t *testing.T) {
	assertError := func(t testing.TB, err error) {
		t.Helper()
		if err == nil {
			t.Error("Want an Error, But didn't get one")
		}
	}
	assertNoError := func(t testing.TB, err error) {
		t.Helper()
		if err != nil {
			t.Error("An error occurring")
		}
	}
	t.Run("Test Deposit,", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(10))

		got := wallet.Balance().String()
		want := Bitcoin(10).String()

		assertBitCoinString(t, got, want)
	})
	t.Run("Test Withdraw,", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(100))
		err := wallet.Withdraw(Bitcoin(15))

		got := wallet.Balance().String()
		want := Bitcoin(85).String()

		assertBitCoinString(t, got, want)
		assertNoError(t, err)
	})
	t.Run("Withdraw insufficient amount", func(t *testing.T) {
		startingBalance := 20
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(startingBalance))

		err := wallet.Withdraw(Bitcoin(startingBalance + 10))

		balance := wallet.Balance()
		want := Bitcoin(startingBalance)

		assertBitCoin(t, balance, want)

		assertError(t, err)

	})
}

func assertBitCoinString(t testing.TB, got string, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got %s want %s", got, want)
	}
}

func assertBitCoin(t testing.TB, got Bitcoin, want Bitcoin) {
	t.Helper()

	if got != want {
		t.Errorf("got %d want %d", got, want)
	}
}

func assertInt(t testing.TB, got int, want int) {
	t.Helper()

	if got != want {
		t.Errorf("got %d want %d", got, want)
	}
}
