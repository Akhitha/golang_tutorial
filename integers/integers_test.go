package integers

import "testing"
import "fmt"

func TestAdd(t *testing.T){
	sum:=Add(2,3)
	expect:= 5
	assertCorrectInt(t, sum, expect)
}

func ExampleAdd() {
	sum := Add(1, 5)
	fmt.Println(sum)
	// Output: 6
}

func assertCorrectInt(t testing.TB, num int,  expectInt int){
	t.Helper()
	if num != expectInt {
		t.Errorf("got %d expected %d", num, expectInt)
	}
}

