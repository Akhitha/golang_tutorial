package arrays

import (
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {

	t.Run("Collection of 5 numbers", func(t *testing.T) {
		numbers := [5]int{1, 2, 3, 4, 5}

		sum := Sum(numbers)
		want := 15
		assertInt(t, sum, want)
	})
	t.Run("Collection of any number of elements", func(t *testing.T) {
		numbers := []int{1, 2, 3}

		sum := SumCollection(numbers)
		want := 6
		assertInt(t, sum, want)
	})

}

func TestSumAll(t *testing.T) {
	got := SumAll([]int{1, 2}, []int{3, 4})
	want := []int{3, 7}

	assertCollection(t, got, want)

}

func TestSumAllTails(t *testing.T) {
	t.Run("Check tail sum with nun empty", func(t *testing.T) {
		got := SumAllTails([]int{0, 2}, []int{4, 8})
		want := []int{2, 8}

		assertCollection(t, got, want)
	})
	t.Run("Check tail sum with empty collections", func(t *testing.T) {
		got := SumAllTails([]int{}, []int{1, 5, 4}, []int{6, 7})
		want := []int{0, 9, 7}

		assertCollection(t, got, want)
	})
}

func assertInt(t testing.TB, got int, want int) {
	t.Helper()

	if got != want {
		t.Errorf("got %d want %d", got, want)
	}
}

func assertCollection(t testing.TB, got []int, want []int) {
	t.Helper()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}
